package id.equity.portalpartner.service.portalclaim;

import lombok.Data;

import java.util.List;

@Data
public class PortalClaimBaseRequest<T> {
    private String username;
    private String password;
    private String app_code;
    private String action;
    private List<Condition> condition;
    private T values;
    private String sort_by;

    public PortalClaimBaseRequest(String username, String password, String app_code, String action, List<Condition> conditions, String sort_by) {
        this.username = username;
        this.password = password;
        this.app_code = app_code;
        this.action = action;
        this.condition = conditions;
        this.sort_by = sort_by;
    }

    public PortalClaimBaseRequest(String username, String password, String app_code, String action, T values) {
        this.username = username;
        this.password = password;
        this.app_code = app_code;
        this.action = action;
        this.values = values;
    }

    public PortalClaimBaseRequest(String username, String password, String app_code, String action, List<Condition> condition, T values) {
        this.username = username;
        this.password = password;
        this.app_code = app_code;
        this.action = action;
        this.condition = condition;
        this.values = values;
    }
}
