package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.ClaimPartnerRead;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimPartnerReadRepository extends PagingAndSortingRepository<ClaimPartnerRead, Long> {
    ClaimPartnerRead findByClaimRegNoAndClaimStatusCodeAndPartnerAndIsRead(String claimRegNo, String claimStatusCode, String partner, Boolean read);
    List<ClaimPartnerRead> findByClaimStatusCodeAndPartnerAndIsRead(String claimStatusCode, String partner, Boolean read);
}
