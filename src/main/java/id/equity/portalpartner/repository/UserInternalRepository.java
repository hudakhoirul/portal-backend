package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.UserInternal;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserInternalRepository extends PagingAndSortingRepository<UserInternal, Long> {
}
