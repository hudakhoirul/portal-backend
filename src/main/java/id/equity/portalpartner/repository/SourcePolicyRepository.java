package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.SourcePolicy;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SourcePolicyRepository extends PagingAndSortingRepository<SourcePolicy, Long> {
}
