package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.ClaimToExpired;
import id.equity.portalpartner.model.enums.ClaimToExpiredStatus;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface ClaimToExpiredRepository extends PagingAndSortingRepository<ClaimToExpired, Long> {
    List<ClaimToExpired> findByExpiredAtLessThanAndStatus(Date currentDate, ClaimToExpiredStatus status);
    ClaimToExpired findByClaimRegNo(String claimRegNo);
}
