package id.equity.portalpartner.repository;

import id.equity.portalpartner.model.ClaimDocumentFile;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClaimDocumentFileRepository extends PagingAndSortingRepository<ClaimDocumentFile, Long> {
}
