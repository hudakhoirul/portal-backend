package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.companyclaims.CompanyClaimDto;
import id.equity.portalpartner.dto.companyclaims.CreateCompanyClaimDto;
import id.equity.portalpartner.dto.companyclaims.SingleCompanyClaimDto;
import id.equity.portalpartner.dto.portalclaim.ClaimTypeDto;
import id.equity.portalpartner.model.CompanyClaim;
import id.equity.portalpartner.model.CompanyPolicy;
import id.equity.portalpartner.repository.CompanyClaimRepository;
import id.equity.portalpartner.repository.CompanyPolicyRepository;
import id.equity.portalpartner.service.portalclaim.PortalClaimService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/company-claims")
public class CompanyClaimController {
    @Autowired
    private CompanyClaimRepository companyClaimRepository;

    @Autowired
    private CompanyPolicyRepository companyPolicyRepository;

    @Autowired
    private PortalClaimService portalClaimService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<CompanyClaimDto>>> find(
            @RequestParam(name = "policy_no", required = false) String policyNo,
            @RequestParam(name = "claim_type_code", required = false) String claimTypeCode
    ) throws IOException, ResourceNotFoundException {
        Iterable<CompanyClaim> companyClaims;

        if (policyNo != null && claimTypeCode != null) {
            CompanyPolicy companyPolicies = companyPolicyRepository.findByPolicyNo(policyNo)
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", policyNo)));
            List<ClaimTypeDto> claimTypeDtos = portalClaimService.getListClaimType(
                    null,
                    true,
                    new HashMap<String, String>() {{
                        put("claim_type_code", claimTypeCode);
                    }}
            );
            companyClaims = companyClaimRepository.findByCompanyPolicyIdAndClaimType(
                    companyPolicies.getId(),
                    claimTypeDtos.get(0).getClaimTypeCode()
            );
        } else {
            companyClaims = companyClaimRepository.findAll();
        }

        Type targetListType = new TypeToken<List<CompanyClaimDto>>() {}.getType();
        List<CompanyClaimDto> companyClaimDtos = modelMapper.map(companyClaims, targetListType);

        BaseResponse<List<CompanyClaimDto>> compaListBaseResponse =
                new BaseResponse<>(true, companyClaimDtos, "Company claim retrieved successfully");

        return ResponseEntity.ok(compaListBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<CompanyClaimDto>> create(@Valid @RequestBody CreateCompanyClaimDto createCompanyClaimDto) throws ResourceNotFoundException {
        CompanyPolicy companyPolicy = companyPolicyRepository.findById(createCompanyClaimDto.getPolicyId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createCompanyClaimDto.getPolicyId())));
        CompanyClaim companyClaimDto = modelMapper.map(createCompanyClaimDto, CompanyClaim.class);
        companyClaimDto.setCompanyPolicy(companyPolicy);
        CompanyClaim companyClaim = companyClaimRepository.save(companyClaimDto);
        SingleCompanyClaimDto singleCompanyClaimDto = modelMapper.map(companyClaim, SingleCompanyClaimDto.class);

        BaseResponse<CompanyClaimDto> companyClaimBaseResponse =
                new BaseResponse(true, singleCompanyClaimDto, "Company claim created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(companyClaimBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyClaimDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        CompanyClaim companyClaim = companyClaimRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        CompanyClaimDto companyClaim1 = modelMapper.map(companyClaim, CompanyClaimDto.class);

        BaseResponse<CompanyClaimDto> companyClaimBaseResponse =
                new BaseResponse(true, companyClaim1, "Company claim retrieved successfully");

        return ResponseEntity.ok(companyClaimBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyClaimDto>> patch(@PathVariable Long id, @Valid @RequestBody CreateCompanyClaimDto createCompanyClaimDto)throws ResourceNotFoundException {
        CompanyPolicy companyPolicy = companyPolicyRepository.findById(createCompanyClaimDto.getPolicyId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createCompanyClaimDto.getPolicyId())));
        companyClaimRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));

        CompanyClaim companyClaim = modelMapper.map(createCompanyClaimDto, CompanyClaim.class);
        companyClaim.setId(id);
        companyClaim.setCompanyPolicy(companyPolicy);

        companyClaimRepository.save(companyClaim);

        CompanyClaimDto companyClaimDto = modelMapper.map(companyClaim, CompanyClaimDto.class);
        BaseResponse<CompanyClaimDto> companyClaimDtoBaseResponse =
                new BaseResponse(true, companyClaimDto, "Company claim patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(companyClaimDtoBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyClaimDto>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        CompanyClaim companyClaim1 = companyClaimRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        CompanyClaimDto companyClaim = modelMapper.map(companyClaim1, CompanyClaimDto.class);

        companyClaimRepository.delete(companyClaim1);

        BaseResponse<CompanyClaimDto> companyClaimDtoBaseResponse =
                new BaseResponse<>(true, companyClaim, "Company claim deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(companyClaimDtoBaseResponse);
    }

}
