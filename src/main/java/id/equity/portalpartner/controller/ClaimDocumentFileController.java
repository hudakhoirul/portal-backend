package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.claimdocumentfile.ClaimDocumentFileDto;
import id.equity.portalpartner.model.ClaimDocumentFile;
import id.equity.portalpartner.repository.ClaimDocumentFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/claim-document-files")
public class ClaimDocumentFileController {
    @Autowired
    private ClaimDocumentFileRepository claimDocumentFileRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ClaimDocumentFileDto>>> find() {
        Iterable<ClaimDocumentFile> companies = claimDocumentFileRepository.findAll();
        Type targetListType = new TypeToken<List<ClaimDocumentFileDto>>() {}.getType();
        List<ClaimDocumentFileDto> claimDocumentFile = modelMapper.map(companies, targetListType);
        BaseResponse<List<ClaimDocumentFileDto>> listBaseResponse =
                new BaseResponse<>(true, claimDocumentFile, "Claim document file retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<ClaimDocumentFile>> create(@Valid @RequestBody ClaimDocumentFileDto claimDocumentFileDto) {
        ClaimDocumentFile claimDocumentFile = modelMapper.map(claimDocumentFileDto, ClaimDocumentFile.class);
        ClaimDocumentFile claimDocumentFile1 = claimDocumentFileRepository.save(claimDocumentFile);
        BaseResponse<ClaimDocumentFile> claimDocumentFileBaseResponse =
                new BaseResponse<>(true, claimDocumentFile1, "Claim document file created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(claimDocumentFileBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimDocumentFileDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimDocumentFile claimDocumentFile = claimDocumentFileRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimDocumentFileDto claimDocumentFile1 = modelMapper.map(claimDocumentFile, ClaimDocumentFileDto.class);
        BaseResponse<ClaimDocumentFileDto> claimDocumentFileBaseResponse =
                new BaseResponse<>(true, claimDocumentFile1, "Claim document file retrieved successfully");

        return ResponseEntity.ok(claimDocumentFileBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimDocumentFile>> patch(@PathVariable Long id, @Valid @RequestBody ClaimDocumentFileDto claimDocumentFileDto) throws ResourceNotFoundException {
        claimDocumentFileRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimDocumentFile claimDocumentFile = modelMapper.map(claimDocumentFileDto, ClaimDocumentFile.class);
        claimDocumentFile.setId(id);

        ClaimDocumentFile claimDocumentFile1 = claimDocumentFileRepository.save(claimDocumentFile);
        BaseResponse<ClaimDocumentFile> claimDocumentFileBaseResponse =
                new BaseResponse<>(true, claimDocumentFile1, "Claim document file patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(claimDocumentFileBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimDocumentFile>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimDocumentFile claimDocumentFile = claimDocumentFileRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimDocumentFile claimDocumentFile1 = modelMapper.map(claimDocumentFile, ClaimDocumentFile.class);

        claimDocumentFileRepository.delete(claimDocumentFile1);

        BaseResponse<ClaimDocumentFile> claimDocumentFileBaseResponse =
                new BaseResponse<>(true, claimDocumentFile1, "Claim document file deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(claimDocumentFileBaseResponse);
    }
}
