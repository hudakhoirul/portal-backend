package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.companypolicy.CompanyPolicyDto;
import id.equity.portalpartner.dto.companypolicy.CreateCompanyPolicyDto;
import id.equity.portalpartner.model.ClaimFlow;
import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.model.CompanyPolicy;
import id.equity.portalpartner.model.SourcePolicy;
import id.equity.portalpartner.repository.ClaimFlowRepository;
import id.equity.portalpartner.repository.CompanyPolicyRepository;
import id.equity.portalpartner.repository.CompanyRepository;
import id.equity.portalpartner.repository.SourcePolicyRepository;
import id.equity.portalpartner.service.CompanyPolicyService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/company-policies")
public class CompanyPolicyController {
    @Autowired
    private CompanyPolicyRepository companyPolicyRepository;

    @Autowired
    private SourcePolicyRepository sourcePolicyRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ClaimFlowRepository claimFlowRepository;

    @Autowired
    private CompanyPolicyService companyPolicyService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<CompanyPolicyDto>>> find(
            @RequestParam(required = false, defaultValue = "") String name,
            @RequestParam(required = false) Long company_id,
            @RequestParam(required = false, defaultValue = "false", name = "is_deleted") Boolean isDeleted
    ) throws ResourceNotFoundException {
        Company company;
        Iterable<CompanyPolicy> companyPolicies;

        if (company_id != null) {
            company = companyRepository.findById(company_id)
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", company_id)));

            companyPolicies = companyPolicyService.findCompanyPolicyByNameAndCompany(name, company, isDeleted);
        }
        else {
            companyPolicies = companyPolicyRepository.findAll();
        }

        Type targetListType = new TypeToken<List<CompanyPolicyDto>>() {}.getType();
        List<CompanyPolicyDto> mapped = modelMapper.map(companyPolicies, targetListType);
        BaseResponse<List<CompanyPolicyDto>> listCompanyPolicy =
                new BaseResponse<>(true, mapped, "Company policy retrieved successfully");

        return ResponseEntity.ok(listCompanyPolicy);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<CompanyPolicyDto>> create(
            @Valid
            @RequestBody CreateCompanyPolicyDto createCompanyPolicyDto
    ) throws ResourceNotFoundException {
        SourcePolicy sourcePolicy = sourcePolicyRepository.findById(createCompanyPolicyDto.getSourceId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createCompanyPolicyDto.getSourceId())));
        Company company = companyRepository.findById(createCompanyPolicyDto.getCompanyId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createCompanyPolicyDto.getCompanyId())));
        ClaimFlow claimFlow = claimFlowRepository.findById(createCompanyPolicyDto.getFlowId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createCompanyPolicyDto.getFlowId())));
        CompanyPolicy mapped = modelMapper.map(createCompanyPolicyDto, CompanyPolicy.class);
        mapped.setSourcePolicy(sourcePolicy);
        mapped.setCompany(company);
        mapped.setClaimFlow(claimFlow);

        CompanyPolicy companyPolicy = companyPolicyRepository.save(mapped);
        CompanyPolicyDto companyPolicy1 = modelMapper.map(companyPolicy, CompanyPolicyDto.class);
        BaseResponse<CompanyPolicyDto> companyPolicyBaseResponse =
                new BaseResponse<>(true, companyPolicy1, "Company policy created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(companyPolicyBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyPolicyDto>> get(@PathVariable String id) throws ResourceNotFoundException {
        CompanyPolicy companyPolicy = companyPolicyRepository.findByPolicyNo(id)
                .orElseGet(() -> {
                    try {
                        return companyPolicyRepository.findById(Long.valueOf(id)).orElse(null);
                    } catch (NumberFormatException e) {
                        return null;
                    }
                });

        if (companyPolicy == null) {
            throw new ResourceNotFoundException(String.format("Resource with id '%s' not found", id));
        }

        CompanyPolicyDto companyPolicy1 = modelMapper.map(companyPolicy, CompanyPolicyDto.class);
        BaseResponse<CompanyPolicyDto> companyPolicyBaseResponse =
                new BaseResponse<>(true, companyPolicy1, "Company policy retrieved successfully");

        return ResponseEntity.ok(companyPolicyBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyPolicy>> patch(@PathVariable Long id, @Valid @RequestBody CreateCompanyPolicyDto companyPolicyDto) throws ResourceNotFoundException {
        companyPolicyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        CompanyPolicy companyPolicy = modelMapper.map(companyPolicyDto, CompanyPolicy.class);
        companyPolicy.setId(id);
        SourcePolicy sourcePolicy = sourcePolicyRepository.findById(companyPolicy.getSourcePolicy().getId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", companyPolicy.getSourcePolicy().getId())));
        Company company = companyRepository.findById(companyPolicy.getCompany().getId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", companyPolicy.getCompany().getId())));
        ClaimFlow claimFlow = claimFlowRepository.findById(companyPolicy.getClaimFlow().getId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", companyPolicy.getClaimFlow().getId())));
        companyPolicy.setSourcePolicy(sourcePolicy);
        companyPolicy.setCompany(company);
        companyPolicy.setClaimFlow(claimFlow);

        CompanyPolicy companyPolicy1 = companyPolicyRepository.save(companyPolicy);
        BaseResponse<CompanyPolicy> companyPolicyBaseResponse =
                new BaseResponse<>(true, companyPolicy1, "Company policy patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(companyPolicyBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<CompanyPolicy>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        CompanyPolicy companyPolicy = companyPolicyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        CompanyPolicy companyPolicy1 = modelMapper.map(companyPolicy, CompanyPolicy.class);

        companyPolicyRepository.delete(companyPolicy);

        BaseResponse<CompanyPolicy> companyPolicyBaseResponse =
                new BaseResponse<>(true, companyPolicy1, "Company policy patched successfully");

        return ResponseEntity.status(HttpStatus.OK).body(companyPolicyBaseResponse);
    }
}
