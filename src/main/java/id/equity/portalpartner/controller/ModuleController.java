package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.modul.CreateModuleDto;
import id.equity.portalpartner.dto.modul.ModuleDto;
import id.equity.portalpartner.model.Application;
import id.equity.portalpartner.model.Module;
import id.equity.portalpartner.repository.ApplicationRepository;
import id.equity.portalpartner.repository.ModuleRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/modules")
public class ModuleController {
    @Autowired
    private ModuleRepository moduleRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ModuleDto>>> find(
            @RequestParam(name = "group_id", required = false) Long groupId
    ) {
        Iterable<Module> modules;

        if (groupId == null) {
            modules = moduleRepository.findAll();
        } else {
            modules = moduleRepository.findByGroupsId(groupId);
        }

        Type targetListType = new TypeToken<List<ModuleDto>>() {}.getType();
        List<ModuleDto> company = modelMapper.map(modules, targetListType);
        BaseResponse<List<ModuleDto>> listBaseResponse =
                new BaseResponse<>(true, company, "Module retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Module>> create(@Valid @RequestBody CreateModuleDto createModuleDto) throws ResourceNotFoundException {
        Application application = applicationRepository.findById(createModuleDto.getAppId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", createModuleDto.getAppId())));
        Module module = modelMapper.map(createModuleDto, Module.class);
        module.setApplication(application);

        Module module1 = moduleRepository.save(module);
        BaseResponse<Module> companyBaseResponse =
                new BaseResponse<>(true, module1, "Module created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(companyBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Module>> get(@PathVariable Long id) throws ResourceNotFoundException {
        Module module = moduleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Module module1 = modelMapper.map(module, Module.class);
        BaseResponse<Module> moduleBaseResponse =
                new BaseResponse<>(true, module1, "Module retrieved successfully");

        return ResponseEntity.ok(moduleBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<Module>> patch(@PathVariable Long id, @Valid @RequestBody ModuleDto moduleDto) throws ResourceNotFoundException {
        Module module = moduleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        module.setId(id);

        Module updated = moduleRepository.save(module);

        BaseResponse<Module> moduleBaseResponse =
                new BaseResponse<>(true, updated, "Module patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(moduleBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Module>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        Module module = moduleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Module module1 = modelMapper.map(module, Module.class);

        moduleRepository.delete(module1);

        BaseResponse<Module> moduleBaseResponse =
                new BaseResponse<>(true, module1, "Module deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(moduleBaseResponse);
    }
}
