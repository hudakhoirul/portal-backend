package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.claimflowtype.ClaimFlowTypeDto;
import id.equity.portalpartner.dto.claimflowtype.ListClaimFlowTypeDto;
import id.equity.portalpartner.model.ClaimFlowType;
import id.equity.portalpartner.repository.ClaimFlowTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/claim-flow-types")
public class ClaimFlowTypeController {
    @Autowired
    private ClaimFlowTypeRepository claimFlowTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ListClaimFlowTypeDto>>> find(
            @RequestParam(name = "claim_flow_id", required = false) Long claimFlowId
    ) {
        Iterable<ClaimFlowType> claimFlowTypes;

        if (claimFlowId != null) {
            claimFlowTypes = claimFlowTypeRepository.findByClaimFlowId(claimFlowId);
        } else {
            claimFlowTypes = claimFlowTypeRepository.findAll();
        }

        Type targetListType = new TypeToken<List<ListClaimFlowTypeDto>>() {}.getType();
        List<ListClaimFlowTypeDto> claimFlowType = modelMapper.map(claimFlowTypes, targetListType);
        BaseResponse<List<ListClaimFlowTypeDto>> listBaseResponse =
                new BaseResponse<>(true, claimFlowType, "Claim flow type retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<ClaimFlowType>> create(@Valid @RequestBody ClaimFlowTypeDto claimFlowTypeDto) {
        ClaimFlowType claimFlowType = modelMapper.map(claimFlowTypeDto, ClaimFlowType.class);
        ClaimFlowType claimFlowType1 = claimFlowTypeRepository.save(claimFlowType);
        BaseResponse<ClaimFlowType> claimFlowTypeBaseResponse =
                new BaseResponse<>(true, claimFlowType1, "Claim flow type created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(claimFlowTypeBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlowTypeDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimFlowType claimFlowType = claimFlowTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlowTypeDto claimFlowType1 = modelMapper.map(claimFlowType, ClaimFlowTypeDto.class);
        BaseResponse<ClaimFlowTypeDto> claimFlowTypeBaseResponse =
                new BaseResponse<>(true, claimFlowType1, "Claim flow type retrieved successfully");

        return ResponseEntity.ok(claimFlowTypeBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlowType>> patch(@PathVariable Long id, @Valid @RequestBody ClaimFlowTypeDto claimFlowTypeDto) throws ResourceNotFoundException {
        claimFlowTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlowType claimFlowType = modelMapper.map(claimFlowTypeDto, ClaimFlowType.class);
        claimFlowType.setId(id);

        ClaimFlowType claimFlowType1 = claimFlowTypeRepository.save(claimFlowType);
        BaseResponse<ClaimFlowType> claimFlowTypeBaseResponse =
                new BaseResponse<>(true, claimFlowType1, "Claim flow type patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(claimFlowTypeBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlowType>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimFlowType claimFlowType = claimFlowTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlowType claimFlowType1 = modelMapper.map(claimFlowType, ClaimFlowType.class);

        claimFlowTypeRepository.delete(claimFlowType1);

        BaseResponse<ClaimFlowType> claimFlowTypeBaseResponse =
                new BaseResponse<>(true, claimFlowType1, "Claim flow type deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(claimFlowTypeBaseResponse);
    }
}
