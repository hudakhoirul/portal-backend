package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.claimflow.ClaimFlowDto;
import id.equity.portalpartner.model.ClaimFlow;
import id.equity.portalpartner.repository.ClaimFlowRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/claim-flows")
public class ClaimFlowController {
    @Autowired
    private ClaimFlowRepository claimFlowRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ClaimFlowDto>>> find() {
        Iterable<ClaimFlow> companies = claimFlowRepository.findAll();
        Type targetListType = new TypeToken<List<ClaimFlowDto>>() {}.getType();
        List<ClaimFlowDto> claimFlow = modelMapper.map(companies, targetListType);
        BaseResponse<List<ClaimFlowDto>> listBaseResponse =
                new BaseResponse<>(true, claimFlow, "Claim flow retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<ClaimFlow>> create(@Valid @RequestBody ClaimFlowDto claimFlowDto) {
        ClaimFlow claimFlow = modelMapper.map(claimFlowDto, ClaimFlow.class);
        ClaimFlow claimFlow1 = claimFlowRepository.save(claimFlow);
        BaseResponse<ClaimFlow> claimFlowBaseResponse =
                new BaseResponse<>(true, claimFlow1, "Claim flow created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(claimFlowBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlowDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimFlow claimFlow = claimFlowRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlowDto claimFlow1 = modelMapper.map(claimFlow, ClaimFlowDto.class);
        BaseResponse<ClaimFlowDto> claimFlowBaseResponse =
                new BaseResponse<>(true, claimFlow1, "Claim flow retrieved successfully");

        return ResponseEntity.ok(claimFlowBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlow>> patch(@PathVariable Long id, @Valid @RequestBody ClaimFlowDto claimFlowDto) throws ResourceNotFoundException {
        claimFlowRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlow claimFlow = modelMapper.map(claimFlowDto, ClaimFlow.class);
        claimFlow.setId(id);

        ClaimFlow claimFlow1 = claimFlowRepository.save(claimFlow);
        BaseResponse<ClaimFlow> claimFlowBaseResponse =
                new BaseResponse<>(true, claimFlow1, "Claim flow patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(claimFlowBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<ClaimFlow>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        ClaimFlow claimFlow = claimFlowRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ClaimFlow claimFlow1 = modelMapper.map(claimFlow, ClaimFlow.class);

        claimFlowRepository.delete(claimFlow1);

        BaseResponse<ClaimFlow> claimFlowBaseResponse =
                new BaseResponse<>(true, claimFlow1, "Claim flow deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(claimFlowBaseResponse);
    }
}
