package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;

import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.application.ApplicationDto;
import id.equity.portalpartner.dto.application.ListApplicationDto;
import id.equity.portalpartner.model.Application;
import id.equity.portalpartner.repository.ApplicationRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/applications")
public class ApplicationController {
    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<ListApplicationDto>>> find() {
        Iterable<Application> applications = applicationRepository.findAll();
        Type targetListType = new TypeToken<List<ListApplicationDto>>() {}.getType();
        List<ListApplicationDto> mapped = modelMapper.map(applications, targetListType);
        BaseResponse<List<ListApplicationDto>> listApplicationDtoBaseResponse =
            new BaseResponse(true, mapped, "Applications retrieved successfully");

        return ResponseEntity.ok(listApplicationDtoBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Application>> create(@Valid @RequestBody ApplicationDto applicationDto) {
        Application mapped = modelMapper.map(applicationDto, Application.class);
        Application application = applicationRepository.save(mapped);
        BaseResponse<Application> applicationBaseResponse =
            new BaseResponse<>(true, application, "Applications created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(applicationBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<ApplicationDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        Application application = applicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        ApplicationDto application1 = modelMapper.map(application, ApplicationDto.class);
        BaseResponse<ApplicationDto> applicationDtoBaseResponse =
            new BaseResponse<>(true, application1, "Applications retrieved successfully");

        return ResponseEntity.ok(applicationDtoBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<Application>> patch(@PathVariable Long id, @Valid @RequestBody ApplicationDto applicationDto) throws ResourceNotFoundException {
        applicationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Application application = modelMapper.map(applicationDto, Application.class);
        application.setId(id);

        Application application1 = applicationRepository.save(application);
        BaseResponse<Application> applicationBaseResponse =
            new BaseResponse<>(true, application, "Application patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(applicationBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Application>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        Application app = applicationRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Application application = modelMapper.map(app, Application.class);

        applicationRepository.delete(application);

        BaseResponse<Application> applicationBaseResponse =
            new BaseResponse<>(true, application, "Applications deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(applicationBaseResponse);
    }
}
