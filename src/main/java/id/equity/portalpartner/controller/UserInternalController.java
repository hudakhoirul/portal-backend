package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.userinternal.UserInternalDto;
import id.equity.portalpartner.model.UserInternal;
import id.equity.portalpartner.repository.UserInternalRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/users-internals")
public class UserInternalController {
    @Autowired
    private UserInternalRepository userInternalRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<UserInternalDto>>> find() {
        Iterable<UserInternal> companies = userInternalRepository.findAll();
        Type targetListType = new TypeToken<List<UserInternalDto>>() {}.getType();
        List<UserInternalDto> userInternal = modelMapper.map(companies, targetListType);
        BaseResponse<List<UserInternalDto>> listBaseResponse =
                new BaseResponse<>(true, userInternal, "User internal retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<UserInternal>> create(@Valid @RequestBody UserInternalDto userInternalDto) {
        UserInternal userInternal = modelMapper.map(userInternalDto, UserInternal.class);
        UserInternal userInternal1 = userInternalRepository.save(userInternal);
        BaseResponse<UserInternal> userInternalBaseResponse =
                new BaseResponse<>(true, userInternal1, "User internal created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(userInternalBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<UserInternalDto>> get(@PathVariable Long id) throws ResourceNotFoundException {
        UserInternal userInternal = userInternalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserInternalDto userInternal1 = modelMapper.map(userInternal, UserInternalDto.class);
        BaseResponse<UserInternalDto> userInternalBaseResponse =
                new BaseResponse<>(true, userInternal1, "User internal retrieved successfully");

        return ResponseEntity.ok(userInternalBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<UserInternal>> patch(@PathVariable Long id, @Valid @RequestBody UserInternalDto userInternalDto) throws ResourceNotFoundException {
        userInternalRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserInternal userInternal = modelMapper.map(userInternalDto, UserInternal.class);
        userInternal.setId(id);

        UserInternal userInternal1 = userInternalRepository.save(userInternal);
        BaseResponse<UserInternal> userInternalBaseResponse =
                new BaseResponse<>(true, userInternal1, "User internal patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userInternalBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<UserInternal>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        UserInternal userInternal = userInternalRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserInternal userInternal1 = modelMapper.map(userInternal, UserInternal.class);

        userInternalRepository.delete(userInternal1);

        BaseResponse<UserInternal> userInternalBaseResponse =
                new BaseResponse<>(true, userInternal1, "User internal deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(userInternalBaseResponse);
    }
}
