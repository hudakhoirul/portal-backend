package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.policyclaimdocument.CreatePolicyClaimDocumentDto;
import id.equity.portalpartner.dto.policyclaimdocument.PolicyClaimDocumentDto;
import id.equity.portalpartner.model.PolicyClaimDocument;
import id.equity.portalpartner.repository.PolicyClaimDocumentRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/policy-claim-documents")
public class PolicyClaimDocumentController {
    @Autowired
    private PolicyClaimDocumentRepository policyClaimDocumentRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<PolicyClaimDocumentDto>>> find(
            @RequestParam(name = "claim_id", required = false) String claimType
    ) {
        Iterable<PolicyClaimDocument> policyClaimDocuments;

        if (claimType != null) {
            policyClaimDocuments = policyClaimDocumentRepository.findByCompanyClaimClaimTypeOrderByDocumentTypeAsc(claimType);
        } else {
            policyClaimDocuments = policyClaimDocumentRepository.findAll();
        }

        Type targetListType = new TypeToken<List<PolicyClaimDocumentDto>>() {}.getType();
        List<PolicyClaimDocumentDto> policyClaimDocumentDtos = modelMapper.map(policyClaimDocuments, targetListType);
        BaseResponse<List<PolicyClaimDocumentDto>> listBaseResponse =
                new BaseResponse<>(true, policyClaimDocumentDtos, "Policy claim document retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<PolicyClaimDocumentDto>> create(@Valid @RequestBody PolicyClaimDocument policyClaimDocument) {
        PolicyClaimDocument policyClaimDocument1 = policyClaimDocumentRepository.save(policyClaimDocument);

        PolicyClaimDocumentDto policyClaimDocumentDto = modelMapper.map(policyClaimDocument1, PolicyClaimDocumentDto.class);

        BaseResponse<PolicyClaimDocumentDto> policyClaimDocumentDtoBaseResponse =
                new BaseResponse<>(true, policyClaimDocumentDto, "Policy claim document created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(policyClaimDocumentDtoBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimDocumentDto>> get(@PathVariable Integer id)
            throws ResourceNotFoundException {
        PolicyClaimDocument policyClaimDocument = policyClaimDocumentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimDocumentDto policyClaimDocumentDto = modelMapper.map(policyClaimDocument, PolicyClaimDocumentDto.class);

        BaseResponse<PolicyClaimDocumentDto> policyClaimDocumentDtoBaseResponse =
                new BaseResponse<>(true, policyClaimDocumentDto, "Policy claim document retrieved successfully");

        return ResponseEntity.ok(policyClaimDocumentDtoBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimDocument>> patch(@PathVariable Integer id, @Valid @RequestBody CreatePolicyClaimDocumentDto policyClaimDocumentDto)
        throws ResourceNotFoundException {
        policyClaimDocumentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimDocument policyClaimDocument = modelMapper.map(policyClaimDocumentDto, PolicyClaimDocument.class);
        policyClaimDocument.setId(id);

        PolicyClaimDocument policyClaimDocument1 = policyClaimDocumentRepository.save(policyClaimDocument);
        BaseResponse<PolicyClaimDocument> policyClaimDocumentBaseResponse =
                new BaseResponse<>(true, policyClaimDocument1, "Policy claim document patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(policyClaimDocumentBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<PolicyClaimDocument>> remove(@PathVariable Integer id)
            throws ResourceNotFoundException {
        PolicyClaimDocument app = policyClaimDocumentRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        PolicyClaimDocument policyClaimDocument = modelMapper.map(app, PolicyClaimDocument.class);

        policyClaimDocumentRepository.delete(app);
        BaseResponse<PolicyClaimDocument> policyClaimDocumentBaseResponse =
                new BaseResponse<>(true, policyClaimDocument, "Policy claim document deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(policyClaimDocumentBaseResponse);
    }
}
