package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.SftpConfig;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.fileupload.UploadFileResponseDto;
import id.equity.portalpartner.model.ClaimDocumentFile;
import id.equity.portalpartner.model.CompanyClaim;
import id.equity.portalpartner.model.PolicyClaimDocument;
import id.equity.portalpartner.repository.ClaimDocumentFileRepository;
import id.equity.portalpartner.repository.CompanyClaimRepository;
import id.equity.portalpartner.repository.PolicyClaimDocumentRepository;
import id.equity.portalpartner.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/uploads")
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ClaimDocumentFileRepository claimDocumentFileRepository;

    @Autowired
    private CompanyClaimRepository companyClaimRepository;

    @Autowired
    PolicyClaimDocumentRepository policyClaimDocumentRepository;

    @Autowired
    private SftpConfig.UploadGateway uploadGateway;

    public UploadFileResponseDto uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/v1/uploads/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponseDto(fileName, fileDownloadUri, file.getContentType(), file.getSize());
    }

    private File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
        multipart.transferTo(convFile);
        
        return convFile;
    }

    @PostMapping
    public BaseResponse<List<UploadFileResponseDto>> uploadMultipleFile(
            @RequestParam(name = "files") MultipartFile[] files,
            @RequestParam(name = "sftp", required = false, defaultValue = "true") Boolean sftp,
            @RequestParam(name = "claim_id") String claimId,
            @RequestParam(name = "policy_no") String policyNo,
            @RequestParam(name = "document_type") String documentCode
    ) {
        List<UploadFileResponseDto> fileResponseDtoList =  Arrays.asList(files)
                .stream()
                .map(sftp ? this::uploadToSFTP : this::uploadFile)
                .collect(Collectors.toList());

        if (fileResponseDtoList.isEmpty()) {
            return new BaseResponse<>(false, fileResponseDtoList, "Failed to uploads file");
        }

        List<ClaimDocumentFile> claimDocumentFiles = fileResponseDtoList
            .stream()
            .map(f -> {
                CompanyClaim companyClaim = companyClaimRepository.findByCompanyPolicyPolicyNoAndClaimType(policyNo, claimId)
                    .orElse(new CompanyClaim());

                PolicyClaimDocument policyClaimDocument = policyClaimDocumentRepository
                        .findByCompanyClaimIdAndDocumentType(companyClaim.getId(), documentCode);

                ClaimDocumentFile cdf = new ClaimDocumentFile(
                    f.getFileName(),
                    new Date(),
                    1L,
                    policyClaimDocument
                );

                return claimDocumentFileRepository.save(cdf);
            }).collect(Collectors.toList());

        return new BaseResponse<>(true, fileResponseDtoList, "Files uploads successfully");
    }

    @GetMapping("{fileName:.+}")
    public ResponseEntity<Resource> loadFileAsResource(@PathVariable String fileName, HttpServletRequest request) {
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private UploadFileResponseDto uploadToSFTP(MultipartFile file) {
        try {
            File f = multipartToFile(file, file.getOriginalFilename());
            uploadGateway.upload(f);

            UploadFileResponseDto uploadFileResponseDto = new UploadFileResponseDto(
                    file.getOriginalFilename(),
                    "",
                    file.getContentType(),
                    file.getSize()
            );

            return uploadFileResponseDto;
        } catch (IOException err) {
            err.printStackTrace();
            return new UploadFileResponseDto();
        }
    }
}
