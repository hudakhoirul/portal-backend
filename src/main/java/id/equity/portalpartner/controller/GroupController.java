package id.equity.portalpartner.controller;

import id.equity.portalpartner.config.error.ResourceNotFoundException;
import id.equity.portalpartner.config.response.BaseResponse;
import id.equity.portalpartner.dto.group.GroupDto;
import id.equity.portalpartner.model.Group;
import id.equity.portalpartner.repository.GroupRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/groups")
public class GroupController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<BaseResponse<List<GroupDto>>> find() {
        Iterable<Group> companies = groupRepository.findAll();
        Type targetListType = new TypeToken<List<GroupDto>>() {}.getType();
        List<GroupDto> group = modelMapper.map(companies, targetListType);
        BaseResponse<List<GroupDto>> listBaseResponse =
                new BaseResponse<>(true, group, "Group retrieved successfully");

        return ResponseEntity.ok(listBaseResponse);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Group>> create(@Valid @RequestBody GroupDto groupDto) {
        Group group = modelMapper.map(groupDto, Group.class);
        Group group1 = groupRepository.save(group);
        BaseResponse<Group> groupBaseResponse =
                new BaseResponse<>(true, group1, "Group created successfully");

        return ResponseEntity.status(HttpStatus.CREATED).body(groupBaseResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Group>> get(@PathVariable Long id) throws ResourceNotFoundException {
        Group group = groupRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Group group1 = modelMapper.map(group, Group.class);
        BaseResponse<Group> groupBaseResponse =
                new BaseResponse<>(true, group1, "Group retrieved successfully");

        return ResponseEntity.ok(groupBaseResponse);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<Group>> patch(@PathVariable Long id, @Valid @RequestBody GroupDto groupDto) throws ResourceNotFoundException {
        groupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Group group = modelMapper.map(groupDto, Group.class);
        group.setId(id);

        Group group1 = groupRepository.save(group);
        BaseResponse<Group> groupBaseResponse =
                new BaseResponse<>(true, group1, "Group patched successfully");

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(groupBaseResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<Group>> remove(@PathVariable Long id) throws ResourceNotFoundException {
        Group group = groupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        Group group1 = modelMapper.map(group, Group.class);

        groupRepository.delete(group1);

        BaseResponse<Group> groupBaseResponse =
                new BaseResponse<>(true, group1, "Group deleted successfully");

        return ResponseEntity.status(HttpStatus.OK).body(groupBaseResponse);
    }
}
