package id.equity.portalpartner.config.response;

import lombok.Data;

@Data
public class BaseResponse<T> {
    public BaseResponse(boolean ok, T data, String message) {
        this.ok = ok;
        this.data = data;
        this.message = message;
    }

    private boolean ok;
    private T data;
    private String message;
}
