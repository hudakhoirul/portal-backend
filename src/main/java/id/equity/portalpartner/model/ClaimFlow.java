package id.equity.portalpartner.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_claim_flow")
public class ClaimFlow implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String name;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "claimFlow")
    @JsonBackReference
    private Set<CompanyPolicy> companyPolicy;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "claimFlow")
    @JsonBackReference
    private Set<ClaimFlowType> claimFlowTypes;

    public ClaimFlow(String name) {
        this.name = name;
    }
}
