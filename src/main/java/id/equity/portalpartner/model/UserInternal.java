package id.equity.portalpartner.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_user_internal")
public class UserInternal implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, unique = true)
    private String username;

    @Column(name = "password",length = 100)
    private String password;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "view_all_company")
    private boolean viewAllCompany;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "i_user_company",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id")
    )
    private Set<Company> companies;

    public UserInternal(String username, boolean viewAllCompany, Group group, Set<Company> companies) {
        this.username = username;
        this.viewAllCompany = viewAllCompany;
        this.group = group;
        this.companies = companies;
    }
}
