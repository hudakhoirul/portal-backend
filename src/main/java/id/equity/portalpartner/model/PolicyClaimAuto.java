package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_policy_claim_auto")
public class PolicyClaimAuto extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "claim_type")
    private String claimType;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "claim_id")
    private CompanyClaim companyClaim;

    public PolicyClaimAuto(String claimType, boolean isDeleted, CompanyClaim companyClaim) {
        this.claimType = claimType;
        this.isDeleted = isDeleted;
        this.companyClaim = companyClaim;
    }
}
