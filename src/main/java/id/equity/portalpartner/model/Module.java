package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by jupiterzhuo on 20/05/19.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name="m_modul")
public class Module extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String name;

    private Long parent;

    @Column(length = 50)
    private String icon;

    @Column(length = 100)
    private String uri;

    @Column(name = "is_view")
    private boolean isView;

    @Column(name = "is_add")
    private boolean isAdd;

    @Column(name = "is_edit")
    private boolean isEdit;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "claim_status_code")
    private String claimStatusCode;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "app_id")
    private Application application;

    @ManyToMany(mappedBy = "modules", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Group> groups;

    public Module(String name, Long parent, String icon, String uri, boolean isView, boolean isAdd, boolean isEdit, boolean isDeleted, Application application, Set<Group> groups) {
        this.name = name;
        this.parent = parent;
        this.icon = icon;
        this.uri = uri;
        this.isView = isView;
        this.isAdd = isAdd;
        this.isEdit = isEdit;
        this.isDeleted = isDeleted;
        this.application = application;
        this.groups = groups;
    }
}
