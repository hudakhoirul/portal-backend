package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_policy_claim_document")
public class PolicyClaimDocument extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "condition")
    private boolean condition;

    @Column(name = "is_copy_parent")
    private boolean isCopyParent;

    @Column(name = "status")
    private boolean status;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "document_type")
    private String documentType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "claim_id")
    private CompanyClaim companyClaim;

    public PolicyClaimDocument(boolean condition, boolean isCopyParent, boolean status, boolean isDeleted, String documentType, CompanyClaim claimId) {
        this.condition = condition;
        this.isCopyParent = isCopyParent;
        this.status = status;
        this.isDeleted = isDeleted;
        this.documentType = documentType;
        this.companyClaim = claimId;
    }
}
