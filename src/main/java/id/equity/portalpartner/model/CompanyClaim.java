package id.equity.portalpartner.model;

import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_company_claim")
public class CompanyClaim extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "claim_type")
    private String claimType;

    @Column(name = "is_used_by_customer")
    private boolean isUsedByCustomer;

    @Column(name = "is_expired")
    private boolean isExpired;

    @Column(name = "expired_days")
    private Integer expiredDays;

    @Column(name = "is_trigger")
    private boolean isTrigger;

    @Column(name = "is_need_beneficiary_info")
    private boolean isNeedBeneficiaryInfo;

    @Column(name = "is_need_bank_insured")
    private boolean isNeedBankInsured;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "policy_id")
    private CompanyPolicy companyPolicy;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "companyClaim")
    private Set<PolicyClaimAuto> policyClaimAuto;

    public CompanyClaim(Long id, String claimType, boolean isUsedByCustomer, boolean isExpired, Integer expiredDays, boolean isTrigger, boolean isNeedBeneficiaryInfo, boolean isNeedBankInsured, boolean isDeleted, CompanyPolicy companyPolicy, Set<PolicyClaimAuto> policyClaimAuto) {
        this.id = id;
        this.claimType = claimType;
        this.isUsedByCustomer = isUsedByCustomer;
        this.isExpired = isExpired;
        this.expiredDays = expiredDays;
        this.isTrigger = isTrigger;
        this.isNeedBeneficiaryInfo = isNeedBeneficiaryInfo;
        this.isNeedBankInsured = isNeedBankInsured;
        this.isDeleted = isDeleted;
        this.companyPolicy = companyPolicy;
        this.policyClaimAuto = policyClaimAuto;
    }
}
