package id.equity.portalpartner.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.equity.portalpartner.config.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_company")
public class Company extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15, unique = true)
    private String code;

    @Column(length = 100)
    private String name;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "companies")
    @JsonBackReference
    private Set<UserInternal> userInternals;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "company")
    @JsonBackReference
    private Set<UserPartner> userPartners;

    public Company(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
