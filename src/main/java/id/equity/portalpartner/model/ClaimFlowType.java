package id.equity.portalpartner.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_claim_flow_type")
public class ClaimFlowType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "flow_type", length = 100)
    private String flowType;

    @Column(name = "is_flow_exist")
    private boolean isFlowExist;

    @Column(name = "is_edit_information")
    private boolean isEditInformation;

    @Column(name = "is_edit_document")
    private boolean isEditDocument;

    @Column(name = "is_check_document")
    private boolean isCheckDocument;

    @Column(name = "is_input_amount")
    private boolean isInputAmount;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_flow_id")
    private ClaimFlow claimFlow;

    public ClaimFlowType(String flowType, boolean isFlowExist, boolean isEditInformation, boolean isEditDocument, boolean isCheckDocument, boolean isInputAmount, ClaimFlow claimFlow) {
        this.flowType = flowType;
        this.isFlowExist = isFlowExist;
        this.isEditInformation = isEditInformation;
        this.isEditDocument = isEditDocument;
        this.isCheckDocument = isCheckDocument;
        this.isInputAmount = isInputAmount;
        this.claimFlow = claimFlow;
    }
}
