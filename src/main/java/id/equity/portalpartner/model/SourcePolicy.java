package id.equity.portalpartner.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_source_policy")
public class SourcePolicy implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100)
    private String name;

    @Column(length = 100)
    private String code;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "sourcePolicy")
    @JsonBackReference
    private Set<CompanyPolicy> companyPolicy;

    public SourcePolicy(String name, String code, Set<CompanyPolicy> companyPolicy) {
        this.name = name;
        this.code = code;
        this.companyPolicy = companyPolicy;
    }
}
