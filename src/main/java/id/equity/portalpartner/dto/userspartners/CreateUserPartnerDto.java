package id.equity.portalpartner.dto.userspartners;

import lombok.Data;

@Data
public class CreateUserPartnerDto extends UserPartnerDto {
    private String password;
}
