package id.equity.portalpartner.dto.claimdocumentfile;

import lombok.Data;

import java.util.Date;

@Data
public class ClaimDocumentFileDto {
    private Long id;
    private String filename;
    private Date uploadDate;
    private Long uploadBy;
}
