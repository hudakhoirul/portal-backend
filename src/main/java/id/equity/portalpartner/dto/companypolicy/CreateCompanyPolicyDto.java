package id.equity.portalpartner.dto.companypolicy;

import id.equity.portalpartner.model.ClaimFlow;
import id.equity.portalpartner.model.Company;
import id.equity.portalpartner.model.SourcePolicy;
import lombok.Data;

@Data
public class CreateCompanyPolicyDto extends CompanyPolicyDto {
    private Long companyId;
    private Long sourceId;
    private Long flowId;
}
