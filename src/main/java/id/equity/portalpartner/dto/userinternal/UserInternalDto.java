package id.equity.portalpartner.dto.userinternal;

import id.equity.portalpartner.model.Group;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserInternalDto {
    private Long id;
    private String username;
    private boolean isDeleted;
    private boolean viewAllCompany;
    private Group group;
}
