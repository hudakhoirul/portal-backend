package id.equity.portalpartner.dto.userinternal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateUserInternalDto extends UserInternalDto {
    private String password;
}
