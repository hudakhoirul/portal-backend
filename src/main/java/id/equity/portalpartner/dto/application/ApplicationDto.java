package id.equity.portalpartner.dto.application;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ApplicationDto {
    private Long id;
    private String name;
}
