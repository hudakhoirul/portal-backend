package id.equity.portalpartner.dto.companyclaims;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCompanyClaimDto extends CompanyClaimDto {
    private Long policyId;
}
