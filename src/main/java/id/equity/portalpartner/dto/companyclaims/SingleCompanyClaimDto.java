package id.equity.portalpartner.dto.companyclaims;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.equity.portalpartner.model.CompanyPolicy;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SingleCompanyClaimDto extends CompanyClaimDto {
    @JsonBackReference
    private CompanyPolicy companyPolicy;
}
