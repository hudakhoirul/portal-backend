package id.equity.portalpartner.dto.portalclaim;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdatedDocumentFileDto {
    private Integer affected;
}
