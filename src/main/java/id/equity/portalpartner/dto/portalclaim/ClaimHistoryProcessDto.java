package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimHistoryProcessDto {
    @SerializedName("transaction_history_process_id")
    private String transactionHistoryProcessId;
    @SerializedName("claim_reg_no")
    private String claimRegNo;
    @SerializedName("process_date")
    private String processDate;
    @SerializedName("user_type_name")
    private String userTypeName;
    @SerializedName("user_partner_name")
    private String userPartnerName;
    @SerializedName("workflow_template_action_id")
    private String workflowTemplateActionId;
    @SerializedName("action_description")
    private String actionDescription;
    @SerializedName("reason_id")
    private String reasonId;
    private String remarks;
    @SerializedName("created_by")
    private String createdBy;
    @SerializedName("created_by_name")
    private String createdByName;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("modified_by")
    private String modifiedBy;
    @SerializedName("modified_by_name")
    private String modifiedByName;
    @SerializedName("modified_date")
    private String modifiedDate;
    @SerializedName("is_delete")
    private Integer isDelete;
}
