package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DocumentFileDto {
    private String status;
    private String desc;
    @SerializedName("image_data")
    private String imageData;
    @SerializedName("image_width")
    private Integer imageWidth;
    @SerializedName("image_height")
    private Integer imageHeight;
}
