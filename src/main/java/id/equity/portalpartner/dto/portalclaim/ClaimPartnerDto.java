package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimPartnerDto {
    @SerializedName("source_app_id")
    private Long sourceAppId;
    @SerializedName("source_app")
    private String sourceApp;
    @SerializedName("transaction_form_id")
    private String transactionFormId;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("eform_id")
    private String eformId;
    @SerializedName("claim_reg_no")
    private String claimRegNo;
    private String partner;
    @SerializedName("partner_name")
    private String partnerName;
    @SerializedName("id_member_partner")
    private String idMemberPartner;
    @SerializedName("policy_no")
    private String policyNo;
    private String product;
    private String participant;
    @SerializedName("participant_no")
    private String participantNo;
    @SerializedName("virtual_account")
    private String virtualAccount;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("finish_date")
    private String finishDate;
    @SerializedName("id_currency_policy")
    private String idCurrencyPolicy;
    @SerializedName("sum_assured")
    private String sumAssured;
    @SerializedName("phone_no1")
    private String phoneNo1;
    @SerializedName("phone_no2")
    private String phoneNo2;
    @SerializedName("email_address")
    private String emailAddress;
    @SerializedName("event_date")
    private String eventDate;
    @SerializedName("claim_type_code")
    private String claimTypeCode;
    @SerializedName("claim_type_name")
    private String claimTypeName;
    @SerializedName("claim_status_code")
    private String claimStatusCode;
    @SerializedName("claim_status_name")
    private String claimStatusName;
    private String currency;
    @SerializedName("claim_amount")
    private String claimAmount;
    @SerializedName("illness_code")
    private String illnessCode;
    @SerializedName("illness_name")
    private String illnessName;
    @SerializedName("illness_text")
    private String illnessText;
    private String beneficiery;
    @SerializedName("beneficiery_ktp_no")
    private String beneficieryKtpNo;
    @SerializedName("beneficiery_relation_insured_code")
    private String beneficieryRelationInsuredCode;
    @SerializedName("beneficiery_family_card_no")
    private String beneficieryFamilyCardNo;
    private String relationship;
    @SerializedName("relation_insured_code")
    private String relationInsuredCode;
    @SerializedName("partner_relation_text")
    private String partnerRelationText;
    @SerializedName("partner_family_card_no")
    private String partnerFamilyCardNo;
    @SerializedName("partner_account_name")
    private String partnerAccountName;
    @SerializedName("partner_account_no")
    private String partnerAccountNo;
    @SerializedName("partner_currency")
    private String partnerCurrency;
    @SerializedName("partner_bank")
    private String partnerBank;
    @SerializedName("partner_bank_name")
    private String partnerBankName;
    @SerializedName("partner_bank_text")
    private String partnerBankText;
    @SerializedName("id_user_partner")
    private String idUserPartner;
    @SerializedName("id_user_partner_mod")
    private String idUserPartnerMod;
    @SerializedName("id_transaction_parent")
    private String idTransactionParent;
    @SerializedName("claim_currency_code")
    private String claimCurrencyCode;
    @SerializedName("claimant_is_insured")
    private Integer claimantIsInsured;
    @SerializedName("claimant_relation_insured_code")
    private String claimantRelationInsuredCode;
    @SerializedName("claimant_relation_insured_name")
    private String claimantRelationInsuredName;
    @SerializedName("claimant_name")
    private String claimantName;
    @SerializedName("claimant_phone_no")
    private String claimantPhoneNo;
    @SerializedName("claimant_email_address")
    private String claimantEmailAddress;
    @SerializedName("created_by")
    private String createdBy;
    @SerializedName("created_date")
    private String createdDate;
    @SerializedName("modified_by")
    private String modifiedBy;
    @SerializedName("modified_date")
    private String modifedDate;
    @SerializedName("is_delete")
    private Integer isDelete;
    private boolean isRead;
}
