package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class ClaimTransactionDto {
    @SerializedName("source_app")
    private String sourceApp;
    @SerializedName("eform_id")
    private String eformId;
    @SerializedName("participant")
    private String participant;
    @SerializedName("participant_no")
    private String participantNo;
    @SerializedName("mobile_no")
    private String mobileNo;
    @SerializedName("email_address")
    private String emailAddress;
    @SerializedName("virtual_account")
    private String virtualAccount;
    private String partner;
    @SerializedName("partner_name")
    private String partnerName;
    @SerializedName("id_member_partner")
    private String idMemberPartner;
    @SerializedName("policy_no")
    private String policyNo;
    @SerializedName("product_name")
    private String productName;
    @SerializedName("period_start_date")
    private String periodStartDate;
    @SerializedName("period_end_date")
    private String periodEndDate;
    private String remarks;
    @SerializedName("sum_assured_currency_code")
    private String sumAssuredCurrencyCode;
    @SerializedName("sum_assured_amount")
    private String sumAssuredAmount;
    @SerializedName("event_date")
    private String eventDate;
    @SerializedName("claim_currency_code")
    private String claimCurrencyCode;
    @SerializedName("claim_amount")
    private String claimAmount;
    @SerializedName("claim_type_code")
    private String claimTypeCode;
    @SerializedName("illness_code")
    private String illnessCode;
    @SerializedName("illness_text")
    private String illnessText;
    @SerializedName("is_insured")
    private String isInsured;
    @SerializedName("claimant_name")
    private String claimantName;
    @SerializedName("claimant_relation_insured_code")
    private String claimantRelationInsuredCode;
    @SerializedName("claimant_phone_no")
    private String claimantPhoneNo;
    @SerializedName("claimant_email_address")
    private String claimantEmailAddress;
    @SerializedName("beneficiary")
    private String beneficiary;
    @SerializedName("beneficiary_relation_insured_code")
    private String beneficiaryRelationInsuredCode;
    @SerializedName("beneficiery_ktp_no")
    private String beneficiaryKtpNo;
    @SerializedName("beneficiary_family_card_no")
    private String beneficiaryFamilyCardNo;
    @SerializedName("account_no")
    private String accountNo;
    @SerializedName("account_bank_code")
    private String accountBankCode;
    @SerializedName("account_bank_text")
    private String accountBankText;
    @SerializedName("account_currency_code")
    private String accountCurrencyCode;
    @SerializedName("account_name")
    private String accountName;
    @SerializedName("claim_status_code")
    private String claimStatusCode;
    @SerializedName("id_user_partner")
    private String idUserPartner;
    private Set<ClaimTransactionDocumentsDto> documents;

    public ClaimTransactionDto(String sourceApp, String eformId, String participant, String participantNo, String mobileNo, String emailAddress, String virtualAccount, String partner, String partnerName, String idMemberPartner, String policyNo, String productName, String periodStartDate, String periodEndDate, String remarks, String sumAssuredCurrencyCode, String sumAssuredAmount, String eventDate, String claimCurrencyCode, String claimAmount, String claimTypeCode, String illnessCode, String illnessText, String isInsured, String claimantName, String claimantRelationInsuredCode, String claimantPhoneNo, String claimantEmailAddress, String beneficiary, String beneficiaryRelationInsuredCode, String beneficiaryKtpNo, String beneficiaryFamilyCardNo, String accountNo, String accountBankCode, String accountBankText, String accountCurrencyCode, String accountName, String claimStatusCode, String idUserPartner, Set<ClaimTransactionDocumentsDto> documents) {
        this.sourceApp = sourceApp;
        this.eformId = eformId;
        this.participant = participant;
        this.participantNo = participantNo;
        this.mobileNo = mobileNo;
        this.emailAddress = emailAddress;
        this.virtualAccount = virtualAccount;
        this.partner = partner;
        this.partnerName = partnerName;
        this.idMemberPartner = idMemberPartner;
        this.policyNo = policyNo;
        this.productName = productName;
        this.periodStartDate = periodStartDate;
        this.periodEndDate = periodEndDate;
        this.remarks = remarks;
        this.sumAssuredCurrencyCode = sumAssuredCurrencyCode;
        this.sumAssuredAmount = sumAssuredAmount;
        this.eventDate = eventDate;
        this.claimCurrencyCode = claimCurrencyCode;
        this.claimAmount = claimAmount;
        this.claimTypeCode = claimTypeCode;
        this.illnessCode = illnessCode;
        this.illnessText = illnessText;
        this.isInsured = isInsured;
        this.claimantName = claimantName;
        this.claimantRelationInsuredCode = claimantRelationInsuredCode;
        this.claimantPhoneNo = claimantPhoneNo;
        this.claimantEmailAddress = claimantEmailAddress;
        this.beneficiary = beneficiary;
        this.beneficiaryRelationInsuredCode = beneficiaryRelationInsuredCode;
        this.beneficiaryKtpNo = beneficiaryKtpNo;
        this.beneficiaryFamilyCardNo = beneficiaryFamilyCardNo;
        this.accountNo = accountNo;
        this.accountBankCode = accountBankCode;
        this.accountBankText = accountBankText;
        this.accountCurrencyCode = accountCurrencyCode;
        this.accountName = accountName;
        this.claimStatusCode = claimStatusCode;
        this.idUserPartner = idUserPartner;
        this.documents = documents;
    }
}
