package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimTransactionDocumentsDto {
    private String filename;
    @SerializedName("document_code")
    private String documentCode;

    public ClaimTransactionDocumentsDto(String filename, String documentCode) {
        this.filename = filename;
        this.documentCode = documentCode;
    }
}
