package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimTypeDto {
    @SerializedName("claim_type_id")
    private Long claimTypeId;
    @SerializedName("claim_type_code")
    private String claimTypeCode;
    private String name;
    private String alias;
    @SerializedName("status_id")
    private Integer statusId;
    @SerializedName("status_description")
    private String statusDescription;
    @SerializedName("is_delete")
    private Integer isDelete;
}
