package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimDocumentFileDto {
    @SerializedName("transaction_document_id")
    private String transactionDocumentId;
    @SerializedName("transaction_document_file_id")
    private String transactionDocumentFileId;
    @SerializedName("transaction_id")
    private String transactionId;
    @SerializedName("document_type_id")
    private String documentTypeId;
    private String token;
    @SerializedName("final_hashcode")
    private String finalHascode;
    @SerializedName("final_document_index")
    private String finalDocumentIndex;
    @SerializedName("final_document_name")
    private String finalDocumentName;
    @SerializedName("document_name")
    private String documentName;
    @SerializedName("document_url")
    private String documentUrl;
    @SerializedName("hashcode_ori")
    private String hashcodeOri;
    private String hashCode;
    @SerializedName("upload_by")
    private String uploadBy;
    @SerializedName("upload_date")
    private String uploadDate;
    @SerializedName("upload_date_str")
    private String uploadDateStr;
    @SerializedName("next_file_id")
    private String nextFileId;
    @SerializedName("prev_file_id")
    private String prevFileId;
    @SerializedName("document_type")
    private String documentType;
}
