package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EformDto {
    @SerializedName("eform_id")
    private Long eformId;
    @SerializedName("eform_code")
    private String eformCode;
    @SerializedName("transaction_type_id")
    private Integer transactionTypeId;
    @SerializedName("transaction_type_desc")
    private String transactionTypeDesc;
    @SerializedName("transaction_category_desc")
    private String transactionCategoryDesc;
    @SerializedName("is_need_surat_kuasa")
    private String isNeedSuratKuasa;
    private String remarks;
    @SerializedName("status_id")
    private Integer statusId;
    @SerializedName("is_delete")
    private String isDelete;
}
