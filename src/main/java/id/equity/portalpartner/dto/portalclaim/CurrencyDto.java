package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CurrencyDto {
    @SerializedName("currency_id")
    private Long currencyId;
    @SerializedName("currency_code")
    private String currencyCode;
    @SerializedName("description")
    private String description;
    @SerializedName("status_id")
    private Integer statusId;
    @SerializedName("status_description")
    private String statusDescription;
    @SerializedName("is_delete")
    private Integer isDelete;
}
