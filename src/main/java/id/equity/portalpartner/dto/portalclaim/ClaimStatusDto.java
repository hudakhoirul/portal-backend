package id.equity.portalpartner.dto.portalclaim;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimStatusDto {
    @SerializedName("claim_status_id")
    private Long claimStatusId;
    @SerializedName("claim_status_code")
    private String claimStatusCode;
    @SerializedName("status_name")
    private String statusName;
    @SerializedName("is_delete")
    private Integer isDelete;
}
