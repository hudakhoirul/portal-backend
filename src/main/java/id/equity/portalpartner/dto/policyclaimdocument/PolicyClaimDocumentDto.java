package id.equity.portalpartner.dto.policyclaimdocument;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PolicyClaimDocumentDto {
    private Integer id;
    private Long claimId;
    private Long documentType;
    private boolean condition;
    private boolean isCopyParent;
    private boolean status;
    private boolean isDeleted;
}
