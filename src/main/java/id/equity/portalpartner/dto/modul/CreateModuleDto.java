package id.equity.portalpartner.dto.modul;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateModuleDto extends ModuleDto {
    private Long appId;
}
