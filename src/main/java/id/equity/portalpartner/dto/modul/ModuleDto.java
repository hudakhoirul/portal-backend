package id.equity.portalpartner.dto.modul;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ModuleDto {
    private Long id;
    private String name;
    private Long parent;
    private String icon;
    private String uri;
    private boolean isView;
    private boolean isAdd;
    private boolean isEdit;
    private String claimStatusCode;
}
