package id.equity.portalpartner.dto.group;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GroupDto {
    private Long id;
    private String name;
    private boolean isDeleted;
}
