package id.equity.portalpartner.dto.claimflowtype;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClaimFlowTypeDto {
    private Long id;
    private String flowType;
    private boolean isFlowExist;
    private boolean isEditInformation;
    private boolean isEditDocument;
    private boolean isCheckDocument;
    private boolean isInputAmount;
}
