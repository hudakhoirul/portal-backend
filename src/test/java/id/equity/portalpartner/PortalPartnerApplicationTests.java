package id.equity.portalpartner;

import id.equity.portalpartner.config.SftpConfig;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = { "sftp.port = 22" })
public class PortalPartnerApplicationTests {
	@Autowired
	private SftpConfig.UploadGateway gateway;

	private static EmbeddedSftpServer server;

	private static Path sftpFolder;

	public static void startServer() throws Exception {
		server = new EmbeddedSftpServer();
		server.setPort(22);
//		sftpFolder = Files.createTempDirectory("SFTP_UPLOAD_TEST");
        sftpFolder = Paths.get("/Users/novalparinussa/jav-uploads");
		server.afterPropertiesSet();
		server.setHomeFolder(sftpFolder);
		// starting SFTP server
		if (!server.isRunning()) {
			server.start();
		}
	}

	public void cleanSftpFolder() throws IOException {
		Files.walk(sftpFolder).filter(Files::isRegularFile).map(Path::toFile).forEach(File::delete);
	}

	@Test
	public void testUpload() throws IOException {
		assertEquals(1, 1);
	}

	public static void stopServer() {
		if (server.isRunning()) {
			server.stop();
		}
	}

}
