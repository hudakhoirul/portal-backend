FROM maven:3.6.1-jdk-11-slim AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
VOLUME ./.m2 /root/.m2
RUN mvn package

FROM openjdk:11-jdk-slim AS jdk
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/portal-partner-0.0.1-SNAPSHOT.jar /
EXPOSE 10024
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/portal-partner-0.0.1-SNAPSHOT.jar"]
